import QtQuick
import QtQuick.Controls
import QtQuick.Window

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    SwipeView {
        id: view
        currentIndex: 1
        anchors.fill: parent

        Item {
            id: firstPage
            Rectangle {
                width: parent.width
                height: parent.height
                color: "orange" // Change the color as desired
                Text {
                    anchors.centerIn: parent
                    text: "Page 1"
                    font.pixelSize: 24
                    color: "white"
                }
            }
        }

        Item {
            id: secondPage
            Rectangle {
                width: parent.width
                height: parent.height
                color: "White" // Change the color as desired
                Text {
                    anchors.centerIn: parent
                    text: "Page 2"
                    font.pixelSize: 24
                    color: "black"
                }
            }
        }

        Item {
            id: thirdPage
            Rectangle {
                width: parent.width
                height: parent.height
                color: "green"
                Text {
                    anchors.centerIn: parent
                    text: "Page 3"
                    font.pixelSize: 24
                    color: "white"
                }
            }
        }
    }

    PageIndicator {
        id: indicator
        count: view.count
        currentIndex: view.currentIndex
        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
